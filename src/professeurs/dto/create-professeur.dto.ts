import { Column } from 'typeorm';

export class CreateProfesseurDto {

  id: number
  nom: string;

  prenom: string;

  classes: Array<string>;

  matieres: string;
}
