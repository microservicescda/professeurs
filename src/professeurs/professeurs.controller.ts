import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Query,
} from '@nestjs/common';
import { ProfesseursService } from './professeurs.service';
import { CreateProfesseurDto } from './dto/create-professeur.dto';
import { UpdateProfesseurDto } from './dto/update-professeur.dto';

@Controller('professeurs')
export class ProfesseursController {
  constructor(private readonly professeursService: ProfesseursService) {}

  @Post()
  create(@Body() createProfesseurDto: CreateProfesseurDto) {
    return this.professeursService.create(createProfesseurDto);
  }

  @Get()
  findAll() {
    return this.professeursService.findAll();
  }

  @Get('/id/:id')
  findOne(@Param('id') id: string) {
    return this.professeursService.findOne(+id);
  }

  @Get('/name')
  findByName(@Query() data: any) {
    return this.professeursService.findByIdentity(data);
  }

  @Get('/ids')
  findByIds(@Query() data: any) {
    const resu = data.ids.split(',');
    return this.professeursService.findByIds(resu);
  }

  @Patch(':id')
  update(
    @Param('id') id: number,
    @Body() updateProfesseurDto: UpdateProfesseurDto,
  ) {
    console.log('test');
    return this.professeursService.update(id, updateProfesseurDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.professeursService.remove(+id);
  }
}
