import { Injectable } from '@nestjs/common';
import { CreateProfesseurDto } from './dto/create-professeur.dto';
import { UpdateProfesseurDto } from './dto/update-professeur.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Professeur } from './entities/professeur.entity';
import { Repository } from 'typeorm';
import { identite } from './type';

@Injectable()
export class ProfesseursService {
  constructor(
    @InjectRepository(Professeur)
    private readonly repo: Repository<Professeur>,
  ) {}
  create(createProfesseurDto: CreateProfesseurDto) {
    return this.repo.save(createProfesseurDto);
  }

  findAll() {
    return this.repo.find();
  }

  findOne(id: any) {
    return this.repo.findOneBy({ id: id });
  }

  findByIds(ids: string[]) {
    return this.repo.createQueryBuilder().whereInIds(ids).getMany();
  }

  findByIdentity(data: identite) {
    return this.repo.findOneBy(data);
  }

  update(id: number, updateProfesseurDto: UpdateProfesseurDto) {
    return this.repo.update(id, updateProfesseurDto);
  }

  remove(id: number) {
    return this.repo.delete(id);
  }
}
