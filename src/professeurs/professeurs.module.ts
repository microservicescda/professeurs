import { Module } from '@nestjs/common';
import { ProfesseursService } from './professeurs.service';
import { ProfesseursController } from './professeurs.controller';
import { Professeur } from './entities/professeur.entity';
import { TypeOrmModule } from '@nestjs/typeorm';

@Module({
  imports: [TypeOrmModule.forFeature([Professeur])],
  controllers: [ProfesseursController],
  providers: [ProfesseursService],
})
export class ProfesseursModule {}
